################### DECLARATIONS ########################
require './lib/instagram/driver'
require './lib/instagram/user'
require './lib/instagram/data'
require './lib/instagram/log'
require './lib/instagram/action_manager'


module Instagram
	class Base
		attr_reader :logger

		@@home_url = "https://instagram.com"

		def initialize(username, password, actions)
			@user = User.new(username,password)
			@driver = Driver.new()
			@logger = Log.new(@user.username)
			@users_followed = Data.new("users_followed", @user.username)
			@manager = ActionManager.new(actions, Time.now, Instagram::Limits::NEW_ACCOUNT)
		end

		def go_home
			@logger.info_log("Going to Home Page")
			@driver.go_to_page(@@home_url)
			@logger.sleep_log(4)
		end

		def go_to_profile(username)
			@logger.info_log("Going to #{username} profile")
			@driver.go_to_page(@@home_url + "/" + username)
			@logger.sleep_log(4)
		end

		def go_to_tag_page(tag)
			@logger.info_log("**************  Going to tag #{tag} Page ************")
			@driver.go_to_page(@@home_url + "/explore/tags/" + tag + '/' )
			@logger.sleep_log(4)
		end

		def start_action
			while @manager.are_available_actions?
				if @manager.available_to_run?
					follow_from_tag_page(@manager.get_current_action)
					@logger.sleep_log(@manager.get_sleep_time)
					@manager.go_to_next_action
				else
					@logger.info_log(@manager.termination_message)
				end
			end
		end

		def log_in
			#gets reactivated <button class="_dcj9f">Close</button>
			#https://www.instagram.com/#reactivated

			begin	
				go_home

				@logger.info_log("Click Login")
				@driver.element_click(@driver.
					get_element(Instagram::ElementType::LINK, "Log in"))
				@logger.sleep_log(3)

				@driver.send_keys(@driver.get_element(Instagram::ElementType::NAME, "username"),
														 @user.username, false)
				# $driver.find_element(:name, "username").send_keys($username)
				@logger.sleep_log(4)
				@driver.send_keys(@driver.get_element(Instagram::ElementType::NAME, "password"), 
														@user.password, false)
				# $driver.find_element(:name, "password").send_keys("me-encanta-123456") 
				@logger.sleep_log(3)

				@driver.element_click(@driver.
					get_element(Instagram::ElementType::TAG, "button"))
				# $driver.find_element(:tag_name, "button").click
				@logger.sleep_log(10)


			rescue  => e
				@logger.debug_log("Exception Class: #{ e.class.name }")
				@logger.debug_log("Exception Message: #{ e.message }")
				@logger.debug_log("Exception Backtrace: #{ e.backtrace }")
				go_home
			end
		end

		def follow_from_tag_page(tag)
			begin
				need_to_break = false
				go_to_tag_page(tag.name)
				@logger.add_user_to_followed("---TAG: " + tag.name)

				for i in 1..Instagram::Limits::ROM_TAGS_MAX
					if need_to_break
						break
					end

					for j in 1..3
						click_in_picture(i.to_s, j.to_s)

						if is_liked_already?
							need_to_break = true
							@logger.info_log("**Break, picture already liked")
							#complete_action tag.name
							break
						end

						like_pic
						@manager.add_like
						# make_comment(get_a_comment)
						follow_from_tag
						close_pic_window

						@logger.sleep_log(3)
					end
				end
				tag.completed
				@logger.info_log("---- Tag #{tag.name} completed ----")
				@logger.info_log("LIKES: #{@manager.likes}")
				@logger.info_log("FOLLOWS: #{@manager.get_follows_number}")
			rescue  => e
				@logger.debug_log("Exception Class: #{ e.class.name }")
				@logger.debug_log("Exception Message: #{ e.message }")
				@logger.debug_log("Exception Backtrace: #{ e.backtrace }")
				@manager.add_error
				go_home
			end
		end

		def click_in_picture(num1,num2)
			@logger.info_log("Click in picture")
			@driver.element_click(@driver.get_element(Instagram::ElementType::XPATH, '//*[@id="react-root"]/section/main/article/div[2]/div[1]/div['+num1+']/div['+ num2 +']/a'))
			@logger.sleep_log(3)
		end

		def like_pic
			@logger.info_log("Like it")
			@driver.element_click(@driver.get_element(Instagram::ElementType::XPATH, 
								"/html/body/div[3]/div/div[2]/div/article/div[2]/section[1]/a[1]" ))
			@logger.sleep_log(2)
		end

		def follow_from_tag	
			user = @driver.get_element(:class, "_2g7d5").text
			@logger.info_log("***following " + user.to_s)
			follow_button_string = @driver.get_element(Instagram::ElementType::CLASS, "_qv64e").text
			if follow_button_string == 'Follow' && !@users_followed.is_word_in_file?(user, @users_followed.full_name)
				@driver.get_element(Instagram::ElementType::CLASS, "_qv64e").click
				@manager.add_user_followed(user)
				@logger.add_user_to_followed(user)
				@users_followed.insert_into_file(user, @users_followed.full_name)
			end	
			@logger.sleep_log(5)              
		end

		def close_pic_window
			@logger.info_log("close pic window")
			@driver.get_element(Instagram::ElementType::XPATH, "/html/body/div[3]/div/button").click
			#/html/body/div[2]/div/button
			@logger.sleep_log(2)								
		end

		def is_liked_already?
			like_text = @driver.get_element(Instagram::ElementType::XPATH, 
				"/html/body/div[3]/div/div[2]/div/article/div[2]/section[1]/a[1]/span").text
			if like_text == "Unlike"
				return true
			end
			return false
		end
	end
end


# $driver = Selenium::WebDriver.for(:chrome)

# $site_url = "https://instagram.com"
# $google_url = "https://google.com"
# $websites = ["https://google.com", "https://twitter.com/"]

# $username = "lol34856"

# # $followed_file = "followed.txt"
# $users_file = "users.txt"
# $followers_file = "followers.txt"

# $tags = ["venezuelan", "comedy", "fun", "love", "friends", "lol", "funny"]
# # $comments = ["Nice", "Awesome", "💪", "💪💪", "👊", "👊👊", "Nice"]

# $user_to_remove = []
# $followers = []

# $time_to_stop = 0
# $number_of_actions = 0
# $max_number_actions_by_hour = 150



###################### METHODS ###################################

# def get_random_website
# 	n = rand($websites.size)
# 	return $websites[n].to_s
# end

# def get_a_comment
# 	n = rand($comments.size)
# 	return $comments[n].to_s
# end

# def log_in
# 	insert_line
# 	puts "Click Log in"
# 	insert_line
# 	$driver.find_element(:link_text, "Log in").click

# 	puts "Waiting 10 seconds"
# 	sleep(10)

# 	$driver.find_element(:name, "username").send_keys($username)
# 	sleep(3)
# 	$driver.find_element(:name, "password").send_keys("me-encanta-123456") 
# 	sleep(3)

# 	$driver.find_element(:tag_name, "button").click
# 	sleep(20)
# end

# //*[@id="react-root"]/section/main/article/div[2]/div[1]/div[1]/div[1]/a

# //*[@id="react-root"]/section/main/article/div[2]/div[1]/div[1]/div[2]/a

# //*[@id="react-root"]/section/main/article/div[2]/div[1]/div[1]/div[3]/a


# //*[@id="react-root"]/section/main/article/div[2]/div[1]/div[2]/div[1]/a




# def make_comment(comment)
# 	user = $driver.find_element(:class, "_2g7d5").text
# 	follow_button_string = $driver.find_element(:class, "_qv64e").text
# 	if follow_button_string == 'Follow' && !is_user_in_users_file?(user)
# 		puts "click in comment"
# 		$driver.find_element(:xpath, "/html/body/div[3]/div/div/div[2]/div/article/div[2]/section[1]/a[2]").click
# 		sleep(4)

# 		puts "send a comment"
# 		element = $driver.find_element(:xpath, "/html/body/div[3]/div/div/div[2]/div/article/div[2]/section[3]/form/textarea")
# 		element.send_keys(comment)
# 		element.submit
# 		sleep(20)
# 	end
# end

# def record_follow_name(name)
# 	if !File.exist?($followed_file)
# 		file = File.new($followed_file, "w")
# 	else
# 		file = File.open($followed_file, "a")
# 	end
# 		file.puts(name)
# 		file.close

# 	record_in_users_file(name)	
# end

# def record_in_users_file(name)
# 	if !File.exist?($users_file)
# 		file = File.new($users_file, "w")
# 	else
# 		file = File.open($users_file, "a")
# 	end
# 		file.puts(name)
# 		file.close
# end

# def is_user_in_users_file?(user)
# 	if File.exist?($users_file)
# 		if File.readlines($users_file).grep(/#{user}/).any?
# 			return true
# 		end
# 		return false
# 	end
# 	return false	
# end

# def check_number_of_actions_in_hour(b)
# 	if $number_of_actions == $max_number_actions_by_hour
# 		puts "Make num of actions 0"
# 		$number_of_actions = 0

		
# 		time_now = Time.now.to_i
# 		if !($time_to_stop > time_now)
# 			subs = $time_to_stop - time_now
# 			puts "sleeping to get to the hour " + subs.to_s
# 			if b == true
# 				$driver.get(get_random_website)
# 			end
# 			sleep(subs)
# 			$time_to_stop = Time.now.to_i
# 		end 
# 	end
# end

# def follow_from_tag_page_action(tag)
# 	need_to_break = false
# 	go_to_tag_page(tag)

# 	for i in 1..2
# 		if need_to_break
# 			break
# 		end

# 		for j in 1..3
# 			check_number_of_actions_in_hour(false)
# 			click_in_picture(i.to_s, j.to_s)

# 			if is_liked_already?
# 				need_to_break = true
# 				puts "****** Breaking because is already liked"
# 				break
# 			end
# 			like_pic

# 			# make_comment(get_a_comment)

# 			follow_from_tag

# 			close_pic_window
# 			sleep(5)
# 			$number_of_actions = $number_of_actions + 1
# 		end
# 		sleep(5)
# 	end

# 	sleep(12)
# end

# def remove_line(string)

#   # save the content of the file
#   f = File.read("followed.txt")
#   # replace (globally) the search string with the new string
#   new_content = f.gsub(string, '')
#   # open the file again and write the new content to it
#   File.open(f, 'w') { |line| line.puts new_content }
#   puts "remove " + string
# end

##################################################################################



# go_to_home

# log_in

# $time_to_stop = Time.now.to_i + 3600
# $tags.each do |tag|
# 	n = rand(2)
# 	if n == 0
# 		follow_from_tag_page_action(tag)
# 	else
# 		insert_line
# 		puts "Go to google"
# 		insert_line
# 		$driver.get(get_random_website)
# 		sleep(200)

# 		follow_from_tag_page_action(tag)
# 	end
# end

# insert_line
# puts "************ FOLLOWED: " + $user_to_remove + " **********************"
# aquiinsert_line

# #followers
# //*[@id="react-root"]/section/main/article/header/section/ul/li[2]/a

# following
# puts "CLICK IN FOLLOWING"
# $driver.find_element(:xpath, '//*[@id="react-root"]/section/main/article/header/section/ul/li[3]/a').click
# sleep(5)
# insert_line
# puts "Inserting users into user to remove array"
# insert_line
# File.readlines($followed_file).each do |line|
# 	$user_to_remove.push(line)
# end

# $user_to_remove.each do |user|
# 	if user.chomp.empty?
# 		puts "empty"
# 	end
# end
#Aqui insert_line
# puts "**check number of actions"
# check_number_of_actions_in_hour(true)
# puts "Sleep 5 hours"
# sleep(18000) 

# go_to_profile


# puts "unfollowing users in the array"
# insert_line

# $number_of_actions = 0
# $user_to_remove.each do |user|
# 		if !user.chomp.empty?
# 			go_to_user_page(user)
# 			puts "unfollowing user " + user
# 			$driver.find_element(:class, '_r9b8f').click
# 			sleep(8)
# 			$number_of_actions = $number_of_actions + 1
# 		else
# 			puts "empty line"
# 		end
# 		check_number_of_actions_in_hour(true)
# 		sleep(10)
#aqui end	

# insert_line
# puts "deleteing followed file"
# File.delete($followed_file)

# a = ["donpapu_", "liveloveven"]
# a.each do |user|
# 	if File.readlines($users_file).grep(/#{user}/).any?
# 		puts "hello"	
# 	end
# end




# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[1]/div/div[1]/div/div[1]/a

# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[1]/div/div[1]/div/div[1]/a
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[1]/div/div[1]/div/div[1]/a
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[3]/div/div[1]/div/div[1]/a
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[4]/div/div[1]/div/div[1]/a





# https://www.instagram.com/leomessi/followers/
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[1]/div/div[2]/span/button
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[2]/div/div[2]/span/button
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[42]/div/div[2]/span/button
# /html/body/div[4]/div/div/button

# https://www.instagram.com/leomessi/following/
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[1]/div/div[2]/span/button
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[2]/div/div[2]/span/button
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[3]/div/div[2]/span/button
# /html/body/div[4]/div/div/div[2]/div/div[2]/ul/div/li[77]/div/div[2]/span/button
# /html/body/div[4]/div/div/button

