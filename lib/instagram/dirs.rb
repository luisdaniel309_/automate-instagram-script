class Dirs

	def initialize(name, path)
		@name = name
		@path = path
		create_directories
	end

	def create_directories
		create_directory(@path)
		create_directory(get_full_name)

	end

	def create_directory(dir)
		if !Dir.exists?(dir)
			Dir.mkdir(dir)
		end	
	end

	def get_full_name
		@path + "/" + @name + "/"
	end

end