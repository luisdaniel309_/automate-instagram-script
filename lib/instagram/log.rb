require 'logger'
require './lib/instagram/files'
require './lib/instagram/dirs'

module Instagram

	class Log
		include Files
		attr_reader :followed_path
		@@dir_name = './log'
		
		def initialize(username)
			@dir = Dirs.new(username, @@dir_name)
			t = Time.now.strftime("%m-%d-%Y-%H-%M")
			new_dir = @dir.get_full_name + t + '/'
			@dir.create_directory(new_dir)
			@logger = Logger.new new_dir + "log.txt"
			### Create users followed file into new log directory
			@followed_path = new_dir + "followed.txt"
			create_file(@followed_path)
			@logger.datetime_format = '%Y-%m-%d %H:%M:%S '
			@logger.level = Logger::DEBUG
		end

		def info_log(s)
			@logger.info(s)
		end

		def debug_log(s)
			@logger.debug(s)
		end

		def sleep_log(t)
			info_log("Sleep #{t.to_s} seconds")
			sleep(t)
		end

		def add_user_to_followed(user)
			insert_into_file(user, @followed_path)
		end
	end
end