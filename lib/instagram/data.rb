require './lib/instagram/dirs'
require './lib/instagram/files'

module Instagram
	class Data
		include Files
		attr_reader :name
		attr_reader :full_name

		@@dir_name = "./data"

		def initialize(f, username)
			@dir = Dirs.new(username, @@dir_name)
			@name = f
			@full_name = @@dir_name + "/" + username + "/" + @name + ".txt"
			create_file(@full_name)
		end	

	end
end