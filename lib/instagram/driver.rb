require 'selenium-webdriver'

module Instagram

	module ElementType
		LINK = :link_text
		NAME = :name
		CLASS = :class
		TAG = :tag_name
		XPATH = :xpath
	end

	class Driver 

		def initialize()
		     @driver = Selenium::WebDriver.for(:chrome)
		end

		def go_to_page(url)
			@driver.get(url)
		end

		def get_element(type, value)
			@driver.find_element(type, value)
		end

		def element_click(element)
			element.click
		end

		def get_element_text(element)
			element.text
		end

		def send_keys(element, string_to_send, submit)
			if submit
				element.send_keys(string_to_send)
				element.submit
			else
				element.send_keys(string_to_send)
			end
		end
	end

	

end