require './lib/instagram/action'
module Instagram

	module Limits
		NEW_ACCOUNT = 500
		OLD = 1200
		SLEEP_ACTION = 30
		ROM_TAGS_MAX = 2
	end

	class ActionManager
		attr_reader :likes
		attr_reader :started_at
		attr_reader :limit
		attr_reader :termination_message

		@@one_hour = 3600
		@@i = 0
		@@current_action = nil
		@@number_errors = 0
		@@termination_message = ""

		def initialize(actions, time, limit)
			@actions = fill_action_list(actions)
			@started_at = time
			@limit = limit
			@likes= 0
			@follows = []
			@unfollows = []
		end

		def go_to_next_action
			@@i = @@i + 1
		end

		def get_current_action
			@actions[@@i]
		end

		def available_to_run?
			if @@i < @limit
				return true
			elsif @@number_errors >= 5
				@@termination_message = "---------- Error limit reached"
				return false
			end
			@@termination_message = "------ Day limit reached"
			return false 
		end

		def start_over
			@@i = 0
		end

		def get_sleep_time
			Instagram::Limits::SLEEP_ACTION
		end

		def fill_action_list(actions)
			list = []
			actions.each do |a|
				list.push(Action.new(a, Instagram::ActionCategory::TAG))
			end
			return list
		end

		def complete_action(name)
			@actions.each do |a|
				if a.name == name
					a.completed
				end
			end
		end

		def are_available_actions?
			@actions.each do |a|
				if a.completed? == false
					return true
				end
			end
			return false
		end

		def add_error
			@@number_errors = @@number_errors + 1
		end

		def get_follows_number
			@follows.count
		end

		def add_user_followed(user)
			@follows.push(user)
		end

		def add_like
			@likes = @likes + 1
		end
	end
end


# Follow people from tags. 
# Make sure you 

