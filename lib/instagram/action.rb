module Instagram

	module ActionCategory
		TAG = :tag
	end

	class Action
		attr_reader :name
		attr_reader :category
		attr_reader :completed_at

		@@completed = false
		@@times_performed = 0

		def initialize(name, category)
			@name = name
			@category = category
			@completed_at = nil
		end

		def completed
			@completed_at = Time.now
		end

		def completed?
			if @completed_at.nil?
				return false
			end
			return true
		end
	end
end

#follow 800
#likes 600