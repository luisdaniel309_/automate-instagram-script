module Instagram
	module Files

		def create_file(name)
			if !File.exist?(name)
				f = File.new(name, "w")
				f.close
			end
		end

		def insert_into_file(word, file)
			file = File.open(file, "a")
			file.puts(word)
			file.close
		end

		def is_word_in_file?(word, file)
			if File.readlines(file).grep(/#{word}/).any?
				return true
			end
			return false	
		end
	end
end
